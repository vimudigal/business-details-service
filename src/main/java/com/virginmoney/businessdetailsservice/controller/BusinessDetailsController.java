package com.virginmoney.businessdetailsservice.controller;

import com.virginmoney.businessdetailsservice.model.BusinessDetailsMO;
import com.virginmoney.businessdetailsservice.service.BusinessDetailsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Business details controller consists of API's which captures various business details
 *
 * @author vijmudig
 */
@RestController
@Api(tags = {"Business Details"})
@RequestMapping("/api/v1/business-details")
public class BusinessDetailsController {

  @Autowired
  private BusinessDetailsService businessDetailsService;

  @PostMapping("/update")
  @ApiOperation(notes = "Update business details", value = "Update business details", nickname = "updateBusinessDetails", tags = {
      "Business Details"})
  ResponseEntity<BusinessDetailsMO> updateBusinessDetails(@Valid @RequestBody BusinessDetailsMO businessDetailsMO) {
    return new ResponseEntity<BusinessDetailsMO>(
        businessDetailsService.updateBusinessDetails(businessDetailsMO), HttpStatus.OK);
  }

}
