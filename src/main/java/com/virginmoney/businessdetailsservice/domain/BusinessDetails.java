package com.virginmoney.businessdetailsservice.domain;

import javax.persistence.Id;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document("BusinessDetails")
public class BusinessDetails {

  @Id
  private String id;

  private String businessName;

  private boolean startb46;

}
