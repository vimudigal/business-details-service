package com.virginmoney.businessdetailsservice.repository;

import com.virginmoney.businessdetailsservice.domain.BusinessDetails;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * author: vijmudig
 */
@Repository
public interface BusinessDetailsRepository extends MongoRepository<BusinessDetails, String> {

}
