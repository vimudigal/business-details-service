package com.virginmoney.businessdetailsservice.service.impl;

import com.virginmoney.businessdetailsservice.domain.BusinessDetails;
import com.virginmoney.businessdetailsservice.model.BusinessDetailsMO;
import com.virginmoney.businessdetailsservice.repository.BusinessDetailsRepository;
import com.virginmoney.businessdetailsservice.mapping.BusinessDetailsMapper;
import com.virginmoney.businessdetailsservice.service.BusinessDetailsService;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Business details service is the service layer the take cares of business logic
 * that's required to update business details
 *
 * @author vijmudig
 */
@Service
public class BusinessDetailsServiceImpl implements BusinessDetailsService {

  @Autowired
  private BusinessDetailsRepository businessDetailsRepository;

  @Override
  public BusinessDetailsMO updateBusinessDetails(BusinessDetailsMO businessDetailsMO) {
    Optional<BusinessDetails> businessDetailsOptional = businessDetailsRepository
        .findById(businessDetailsMO.getId());
    BusinessDetails businessDetails =
        businessDetailsOptional.isPresent() ? businessDetailsOptional.get() : new BusinessDetails();
    businessDetails.setBusinessName(businessDetailsMO.getBusinessName());

    // Persist the updated record
    businessDetailsRepository.save(businessDetails);

    return BusinessDetailsMapper.INSTANCE.toBusinessDetailsMO(businessDetails);
  }
}
