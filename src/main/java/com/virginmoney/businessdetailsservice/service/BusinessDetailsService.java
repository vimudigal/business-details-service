package com.virginmoney.businessdetailsservice.service;

import com.virginmoney.businessdetailsservice.model.BusinessDetailsMO;

/**
 *
 * author: vijmudig
 */
public interface BusinessDetailsService {

  BusinessDetailsMO updateBusinessDetails(BusinessDetailsMO businessDetailsMO);

}
