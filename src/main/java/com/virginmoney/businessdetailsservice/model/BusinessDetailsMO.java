package com.virginmoney.businessdetailsservice.model;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import lombok.Data;

/**
 *
 * @author vijmudig
 *
 */
@Data
public class BusinessDetailsMO {

  @NotEmpty
  private String id;

  @Valid
  @Size(max = 150, message = "Business name should not be greater than 150")
  private String businessName;

}
