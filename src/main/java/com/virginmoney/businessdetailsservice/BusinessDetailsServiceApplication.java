package com.virginmoney.businessdetailsservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * Business Details Service Application
 *
 * @author vijmudig
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class BusinessDetailsServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(BusinessDetailsServiceApplication.class, args);
  }

}
