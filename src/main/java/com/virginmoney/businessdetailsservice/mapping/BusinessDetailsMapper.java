package com.virginmoney.businessdetailsservice.mapping;

import com.virginmoney.businessdetailsservice.domain.BusinessDetails;
import com.virginmoney.businessdetailsservice.model.BusinessDetailsMO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * Map domain to dto, dto to domain
 *
 * @author vijmudig
 */
@Mapper(componentModel = "spring")
public interface BusinessDetailsMapper {

  BusinessDetailsMapper INSTANCE = Mappers.getMapper(BusinessDetailsMapper.class);

  BusinessDetailsMO toBusinessDetailsMO(BusinessDetails businessDetails);

}
