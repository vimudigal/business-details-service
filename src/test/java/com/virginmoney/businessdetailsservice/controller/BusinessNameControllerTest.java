package com.virginmoney.businessdetailsservice.controller;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.junit.jupiter.MockitoExtension;

import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

/**
 *
 * author: vijmudig
 */
@ExtendWith(MockitoExtension.class)
@ContextConfiguration(classes = { AnnotationConfigContextLoader.class })
@ActiveProfiles("test")
public class BusinessNameControllerTest {

  private static Validator validator;

  @BeforeAll
  public static void setUp() {
    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    validator = factory.getValidator();
  }

  @Test
  void testBusinessNameValid() {

  }

  @Test
  void testBusinessNameMoreThan150Characters() {

  }
}

